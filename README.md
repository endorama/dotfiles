# endorama's dotfiles

[chezmoi](https://www.chezmoi.io/) managed dotfiles folder.


Install Nix


Install chezmoi (temp):
```
$ nix-env -i chezmoi

$ chezmoi init https://gitlab.com/endorama/dotfiles.git
$ chezmoi apply
$ # install home-manager
$ nix-channel --add https://github.com/nix-community/home-manager/archive/release-23.05.tar.gz home-manager
$ nix-channel --update
$ nix-shell '<home-manager>' -A install
$ nix-env -e chezmoi
$ home-manager switch
```
