#
set fisher_path $HOME/.local/share/fisher

function add_path_unless_present -d "Add path to PATH unless already present"
    if not contains $argv $fish_user_paths
        echo "adding $argv"
        set -Ua fish_user_paths $argv
    end
end

add_path_unless_present $HOME/.local/bin
#add_path_unless_present $HOME/bin
add_path_unless_present $HOME/go/bin
#add_path_unless_present $HOME/.dotfiles/bin
#add_path_unless_present "/home/linuxbrew/.linuxbrew/bin" 
#add_path_unless_present "/home/linuxbrew/.linuxbrew/sbin"
