# Fish configurations

This folder contains `fish` shell configurations.

## Load order

All files are loaded alphabetically.

1. `conf.d/*`
1. `config.fish`

## Install fisher

```
curl -sL https://git.io/fisher | source && fisher install jorgebucaran/fisher
```

Docs: [jorgebucaran/fisher](https://github.com/jorgebucaran/fisher)

### Installing plugins after fisher

`$ fisher update`

### Adding plugins

`$ vim $HOME/.config/fish/fish_plugins`

## Help

https://fishshell.com/
https://github.com/jorgebucaran/cookbook.fish
