{ config, pkgs, lib, ... }:
{
  programs.starship = {
    package = pkgs.unstable.starship;
    enable = true;
    
    enableFishIntegration = true;

    # Configuration written to ~/.config/starship.toml
    # https://github.com/search?p=1&q=language%3Anix+programs.starship.settings&type=Code
    # https://nix-community.github.io/home-manager/options.html#opt-programs.starship.settings
    settings = {
      add_newline = false;

      format = lib.concatStrings [
        "[┌ ](grey)"
        "$kubernetes"
        "$vcsh"
        "$git_branch$git_commit$git_state$git_metrics$git_status"
        "$hg_branch"
        "$docker_context"
        "$package"
        "$cmake"
        "$dart"
        "$deno"
        "$dotnet"
        "$elixir"
        "$elm"
        "$erlang"
        "$golang"
        "$helm"
        "$java"
        "$julia"
        "$kotlin"
        "$nim"
        "$nodejs"
        "$ocaml"
        "$perl"
        "$php"
        "$purescript"
        "$python"
        "$red"
        "$ruby"
        "$rust"
        "$scala"
        "$swift"
        "$terraform"
        "$vlang"
        "$vagrant"
        "$zig"
        "$nix_shell"
        "$conda"
        "$memory_usage"
        "$aws"
        "$gcloud"
        "$openstack"
        "$env_var"
        "$crystal"
        "$custom"
        "\n"
        "[│](grey)"
        "$shlvl"
        "$directory"
        "$cmd_duration"
        "\n"
        "[└](grey)"
        "$jobs"
        "$time"
        "$status"
        "$shell"
        "$character"
      ];

      # character = {
      #   success_symbol = "[➜](bold green)";
      #   error_symbol = "[➜](bold red)";
      # };
      # 
      custom = {
        elasticPackage = {
          # elastic-package current profile
          command = "echo $ELASTIC_PACKAGE_PROFILE";
          when = "test -n \"$ELASTIC_PACKAGE_PROFILE\"";
          symbol = "󰏓";
        };
      };

      directory = {
        read_only = " ";
        truncation_length = 10;
        truncation_symbol = "…/";
      };

      env_var = {
        variable = "DEVID_ACTIVE_PERSONA";
        symbol = "🆔";
        style = "fg:purple";
        format = "$symbol [$env_value]($style) ";
      };

      # time = {
      #   disabled = false;
      # };

      # Nerd fonts symbols
      aws = { symbol = "  "; };
      conda = { symbol = " "; };
      dart = { symbol = " "; };
      docker_context = { symbol = " "; };
      elixir = { symbol = " "; };
      elm = { symbol = " "; };
      git_branch = { symbol = " "; };
      golang = { symbol = " "; };
      hg_branch = { symbol = " "; };
      java = { symbol = " "; };
      julia = { symbol = " "; };
      memory_usage = { symbol = "󰍛 "; };
      nim = { symbol = " "; };
      nix_shell = { symbol = " "; };
      package = { symbol = " "; };
      perl = { symbol = " "; };
      php = { symbol = " "; };
      python = { symbol = " "; };
      ruby = { symbol = " "; };
      rust = { symbol = " "; };
      scala = { symbol = " "; };
      shlvl = { symbol = " "; };
      swift = { symbol = "ﯣ "; };
    };
  };
}
