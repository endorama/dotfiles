{ pkgs ? import <nixpkgs> {} }:
let
  nixpkgsUnstableurlUrl = "https://nixos.org/channels/nixos-unstable/nixexprs.tar.xz";
in
{
  allowUnfree = true;

  permittedInsecurePackages = [
    "vault-1.14.10"
  ];

  packageOverrides = pkgs: {
    # add pkgs.unstable key with always up to date unstable channel
    # TODO: remove this as it makes very slow each hmngr switch operation
    unstable = import (fetchTarball nixpkgsUnstableurlUrl) {
      config = {
        allowUnfree = true;
      };
    };

    # add pkgs.custom key with custom packages from the ./packages folder
    custom = {
      # unfree, Elastic License V2
      _elastic-package = pkgs.callPackage ./packages/elastic-package.nix { inherit pkgs; };
      _devenv = pkgs.callPackage ./packages/devenv.nix { inherit pkgs; };

      dep-tree = pkgs.callPackage ./packages/dep-tree.nix {};
      devid = pkgs.callPackage ./packages/devid.nix {};
      go-licenser = pkgs.callPackage ./packages/go-licenser.nix {};
      petname = pkgs.callPackage ./packages/petname.nix {};
      ticker = pkgs.callPackage ./packages/ticker.nix {};
      tickrs = pkgs.callPackage ./packages/tickrs.nix {};
    };
  };
}

