{pkgs, ...}:
{
  # https://nix-community.github.io/home-manager/options.xhtml#opt-programs.zoxide.enable
  programs.zoxide = {
    enable = true; # requires fzf
    enableFishIntegration = true;
    package = pkgs.unstable.zoxide;
  };
}

