{ pkgs ? import <nixpkgs> {}, lib }:

# NOTE: this derivation does not work and maybe never will
# https://discourse.nixos.org/t/git-buildgomodule-private-repositories/5167/8
# The repo can't be downloaded using fetchFromGitHub as usual. The reason is
# that is a private repository:
# https://discourse.nixos.org/t/why-doesnt-fetchfromgithub-support-submodules-with-private/6600/10
# The alternative is to directly fetch the git object with fetchGit (not to be
# confused with fetchgit 😢).
# This works and using modRoot is possible to trigger the build process.
# But in this case the package rely on another package in the same private 
# repo, and there is no way to express this in Nix.
pkgs.buildGoModule rec {
  pname = "oblt-cli";
  version = "7.0.2";

  src = builtins.fetchGit {
    url = "git@github.com:elastic/observability-test-environments.git";
    #rev = "${version}";
    rev = "1bde1bc44d665f677360051e9b48d0068bde1d4e";
    #rev = "adab8b916a45068c044658c4158d81878f9ed1c3";
    ref = "refs/tags/${version}";
    shallow = true;
  };


 # src = pkgs.fetchFromGitHub {
 #   owner = "elastic";
 #   repo = "observability-test-environments";
 #   rev = "${version}";
 #   sha256 = lib.fakeSha256;
 # };

  modRoot = "./tools/oblt-cli";
  vendorHash = "sha256:${lib.fakeSha256}";
  #subPackages = [ "tools/oblt-cli/" ];
  #excludedPackages = ["."];
  #excludedPackages = [".buildkite" "tools"];
  # tests rely on .git folder and network, skipping
  doCheck = false;

  meta = with lib; {
    description = "Grant access to Obs test envs & queries Unified Release process";
    homepage = "https://github.com/elastic/observability-test-environments";
    license = licenses.elastic20;
    platforms = platforms.linux;
    maintainers = with maintainers; [ endorama ];
  };
}
