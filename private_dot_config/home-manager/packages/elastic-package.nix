{ pkgs, lib }:

let
  unstable = import <nixpkgs-unstable> { config = { allowUnfree = true; }; };
in
# pkgs.buildGoModule rec {
unstable.buildGo122Module rec {
  pname = "elastic-package";
  version = "0.100.0";

  src = pkgs.fetchFromGitHub {
    owner = "elastic";
    repo = "elastic-package";
    rev = "v${version}";

    # sha256 = "sha256-AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=";
    sha256 = "sha256-aDprbpdwKLYrNhxrDrTLqwPLF8JjWf7wEA0pZ3MvvHg=";

    leaveDotGit = true;
    postFetch = ''
      cd "$out"
      git rev-parse HEAD > $out/COMMIT
      find "$out" -name .git -print0 | xargs -0 rm -rf
    '';
  };

  # vendorHash = "sha256-AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=";
  vendorHash = "sha256-uWVW4hGME/6u1GnvH1b8jCvza9eJtnH+wyytGe7FYaY=";

  excludedPackages = [".buildkite" "tools"];
  # tests rely on .git folder and network, skipping
  doCheck = false;

  ldflags = [ 
    "-X github.com/elastic/elastic-package/internal/version.Tag=${version}"
  ];

  preBuild = ''
    ldflags+=" -X github.com/elastic/elastic-package/internal/version.CommitHash=$(cat COMMIT)"
    ldflags+=" -X github.com/elastic/elastic-package/internal/version.BuildTime=$(date +%s)"
  '';

  meta = with lib; {
    description = "Command line tool for developing Elastic Integrations";
    homepage = "https://github.com/elastic/elastic-package";
    license = licenses.elastic20;
    platforms = platforms.linux;
    maintainers = with maintainers; [ endorama ];
  };
}
