{ stdenv, buildGoPackage, fetchFromGitHub, lib }:

buildGoPackage rec {
  pname = "petname";
  version = "8e5a1ed0cff0384869564ec1c086c6467a025667";

  goPackagePath = "github.com/dustinkirkland/golang-petname";

  src = fetchFromGitHub {
    owner = "dustinkirkland";
    repo = "golang-petname";
    rev = "${version}";
    # nix-prefetch-url --unpack https://github.com/dustinkirkland/golang-petname/archive/8e5a1ed0cff0384869564ec1c086c6467a025667.tar.gz 
    sha256 = "0znhq3naklirdfnrn97fa4fyy5gvy3bzgn7njs27knzkky0aj32g";
  };

  # modSha256 = "1ydzqg8p2d514sdb34b2p6k1474nr1drrn3gay2cpyhrj5l51cj3";
  # vendorSha256 = null;

  subPackages = [ "cmd/petname" ];

  meta = with lib; {
    description = "Generate pronounceable, sometimes even memorable, pet names";
    homepage = "https://github.com/dustinkirkland/golang-petname";
    license = licenses.asl20;
    # maintainers = with maintainers; [  ];
    platforms = platforms.linux;
  };
}
