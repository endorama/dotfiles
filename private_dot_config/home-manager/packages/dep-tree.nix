{ lib
, buildGoModule
, fetchFromGitHub
}:

buildGoModule rec {
  pname = "dep-tree";
  version = "0.22.4";

  src = fetchFromGitHub {
    owner = "gabotechs";
    repo = "dep-tree";
    rev = "v${version}";
    hash = "sha256-MuLpi2vF/ISbdnOVKUA8O3VugMiuFbFqag7hkA4eouQ=";
    # make fetchFromGitHub aware of version to prevent stale build
    # https://discourse.nixos.org/t/does-fetchfromgithub-ignore-rev-when-a-derivation-with-equivalent-sha256-is-present/8752/7
    name = "${pname}-${version}";
  };

  vendorHash = "sha256-KoVOjZq+RrJ2gzLnANHPPtbEY1ztC0rIXWD9AXAxqMg=";

  # NOTE: tests use the network, skipping them as we are fetching
  # a tag tarball and we expect tests to be successful upstream.
  doCheck = false;

  # ldflags = [
  #   "-s"
  #   "-w"
  #   "-X github.com/endorama/${pname}/internal/version.version=${version}"
  #   "-X github.com/endorama/${pname}/internal/version.commit=${version}"
  # ];

  meta = with lib; {
    description = "Visualising a code base complexity using a 3d force-directed graph";
    homepage = "https://github.com/gabotechs/dep-tree";
    license = licenses.mit;
    maintainers = with maintainers; [ endorama ];
    mainProgram = "dep-tree";
  };
}
