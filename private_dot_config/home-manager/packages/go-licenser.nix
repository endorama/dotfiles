{ pkgs ? import <nixpkgs> {}, lib }:

pkgs.stdenv.mkDerivation rec {
  pname = "go-licenser";
  version = "0.4.0";

  src = pkgs.fetchurl {
    url = "https://github.com/elastic/go-licenser/releases/download/v${version}/go-licenser_${version}_Linux_x86_64.tar.gz";
    sha256 = "1q4ccp8q55vk4kjl9rsl1465h7z7llilpc14bkywpympxfmn472b";
  };
  phases = ["unpackPhase" "installPhase" "patchPhase"];

  unpackPhase = ''
    mkdir ${pname}-${version}
    tar -C ${pname}-${version} -xzf $src
  '';

  installPhase = ''
    runHook preInstall
    mkdir -p $out/bin
    install -Dm755 ${pname}-${version}/go-licenser -t $out/bin
    runHook postInstall
  '';

  meta = with lib; {
    description = "Adds a license header to any Go source files";
    homepage = "https://github.com/elastic/go-licenser";
    license = licenses.asl20;
    # maintainers = with maintainers; [ endorama ];
  };
}

