# { pkgs ? import <nixpkgs> {}, lib }:

# pkgs.buildGoModule {
#   pname = "devid";
#   version = "0.3.0";

#   src = pkgs.fetchFromGitHub {
#     owner = "endorama";
#     repo = "devid";
#     rev = "v0.3.0";
#     sha256 = "";
#   };

#   vendorHash = "";

#   meta = with lib; {
#     description = "Securely manage your developer personas";
#     homepage = "https://github.com/endorama/devid";
#     license = licenses.asl20;
#     platforms = platforms.linux;
#     maintainers = with maintainers; [ endorama ];
#   };
# }
{ lib
, buildGoModule
, fetchFromGitHub
}:

buildGoModule rec {
  pname = "devid";
  version = "0.3.0";

  src = fetchFromGitHub {
    owner = "endorama";
    repo = "devid";
    rev = "v${version}";
    hash = "sha256-22I6PmtDrGqP/Q11RTdk5m6yhxsE0r/oeJPcZIxX7LM=";
    # make fetchFromGitHub aware of version to prevent stale build
    # https://discourse.nixos.org/t/does-fetchfromgithub-ignore-rev-when-a-derivation-with-equivalent-sha256-is-present/8752/7
    name = "${pname}-${version}";
  };

  vendorHash = "sha256-MkzXkS6ukpWd3Et92jugJIAIdJ1oy03X9Jy+opEF9IA=";

  ldflags = [
    "-s"
    "-w"
    "-X github.com/endorama/${pname}/internal/version.version=${version}"
    "-X github.com/endorama/${pname}/internal/version.commit=${version}"
  ];

  meta = with lib; {
    description = "Securely manage your developer personas";
    homepage = "https://github.com/endorama/devid";
    license = licenses.asl20;
    maintainers = with maintainers; [ endorama ];
    mainProgram = "devid";
  };
}
