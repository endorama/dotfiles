{ pkgs ? import <nixpkgs> {}, lib }:

pkgs.stdenv.mkDerivation rec {
  pname = "ticker";
  version = "4.5.6";

  src = pkgs.fetchurl {
    url = "https://github.com/achannarasappa/ticker/releases/download/v${version}/${pname}-${version}-linux-amd64.tar.gz";
    sha256 = "oQqNEs2QEayaGgOabJOPV0SI9kemY3GK5f+SnvyhcNQ=";
  };
  phases = ["unpackPhase" "installPhase" "patchPhase"];

  unpackPhase = ''
    mkdir ${pname}-${version}
    tar -C ${pname}-${version} -xzf $src
  '';

  installPhase = ''
    runHook preInstall
    mkdir -p $out/bin
    install -Dm755 ${pname}-${version}/${pname} -t $out/bin
    runHook postInstall
  '';

  meta = with lib; {
    description = "Terminal stock ticker with live updates and position tracking";
    homepage = "https://github.com/achannarasappa/ticker";
    license = licenses.gpl3;
    # maintainers = with maintainers; [ endorama ];
  };
}

