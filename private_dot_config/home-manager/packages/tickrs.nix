{ pkgs ? import <nixpkgs> {}, lib }:

pkgs.stdenv.mkDerivation rec {
  pname = "tickrs";
  version = "0.14.4";

  src = pkgs.fetchurl {
    url = "https://github.com/tarkah/tickrs/releases/download/v${version}/${pname}-v${version}-x86_64-unknown-linux-gnu.tar.gz";
    sha256 = "17ckwyrzzsskzg4py48w608ynw9dpcvdzxnf9j4kfvl8miv4wfnb";
  };
  phases = ["unpackPhase" "installPhase" "patchPhase"];

  unpackPhase = ''
    mkdir ${pname}-${version}
    tar -C ${pname}-${version} -xzf $src
  '';

  installPhase = ''
    runHook preInstall
    mkdir -p $out/bin
    install -Dm755 ${pname}-${version}/${pname} -t $out/bin
    runHook postInstall
  '';

  meta = with lib; {
    description = "Realtime ticker data in your terminal chart_with_upwards_trend";
    homepage = "https://github.com/tarkah/tickrs";
    license = licenses.mit;
    # maintainers = with maintainers; [ endorama ];
  };
}

