{ config, pkgs, lib, ... }:
let
    readFn = name: builtins.readFile (./. + "/fish/functions/${name}.fish");
 in 
{
  programs.fish = {
    enable = true;

    plugins = [
      # https://github.com/kidonng/nix.fish
      # https://github.com/oh-my-fish/plugin-node-binpath
      # {
      #   name = "jorgebucaran/fisher";
      #   src = pkgs.fetchFromGitHub {
      #     owner = "jorgebucaran";
      #     repo = "fisher";
      #     rev = "4.4.3";
      #     sha256 = "q9Yi6ZlHNFnPN05RpO+u4B5wNR1O3JGIn2AJ3AEl4xs=";
      #   };
      # }
      { name = "fzf"; src = pkgs.fishPlugins.fzf-fish.src; }
      {
        name = "oh-my-fish/plugin-fasd";
        src =  pkgs.fetchFromGitHub { 
          owner = "oh-my-fish";
          repo = "plugin-fasd";
          rev = "98c4c729780d8bd0a86031db7d51a97d55025cf5";
          sha256 = "8JASaNylXAGnWd2IV88juk73b8eJJlVrpyiRZUwHGFQ=";
        };
      }
      { 
        name = "PatrickF1/colored_man_pages.fish";
        src =  pkgs.fetchFromGitHub { 
          owner = "PatrickF1";
          repo = "colored_man_pages.fish";
          rev = "f885c2507128b70d6c41b043070a8f399988bc7a";
          sha256 = "ii9gdBPlC1/P1N9xJzqomrkyDqIdTg+iCg0mwNVq2EU=";
        };
      }
      # { 
      #   name = "";
      #   src =  pkgs.fetchFromGitHub { 
      #     owner = "";
      #     repo = "";
      #     rev = "";
      #     sha256 = "";
      #   };
      # }
      { 
        name = "oh-my-fish/plugin-xdg";
        src =  pkgs.fetchFromGitHub { 
          owner = "oh-my-fish";
          repo = "plugin-xdg";
          rev = "f434591305794c82607a15750aca7d68ac36a6f1";
          sha256 = "0jlyjjvzmx105lx89059dpp4zl5lsn1r4ssczf2xaqz3z59v2f7b";
        };
      }
      { 
        name = "oh-my-fish/plugin-await";
        src =  pkgs.fetchFromGitHub { 
          owner = "oh-my-fish";
          repo = "plugin-await";
          rev = "81bfe1cd1f059712ea7cf743601fb13ed1b28c10";
          sha256 = "13n7b2k86y62mi1z6nnad1ap6mkzkngib2y11pd98py2q3zj213m";
        };
      }
      { 
        name = "oh-my-fish/plugin-title";
        src =  pkgs.fetchFromGitHub { 
          owner = "oh-my-fish";
          repo = "plugin-title";
          rev = "320188bc2ea98ac658a78e927fcb25087d08f22c";
          sha256 = "15f9xrp4b5hfa26v2j428izfcmv47v5kyhbxxa4cyz6s19llsxiv";
        };
      }
      { 
        name = "oh-my-fish/plugin-pbcopy";
        src =  pkgs.fetchFromGitHub { 
          owner = "oh-my-fish";
          repo = "plugin-pbcopy";
          rev = "e8d78bb01f66246f7996a4012655b8ddbad777c2";
          sha256 = "1bmy46mifjbjy9fj2rqiypj94g9ww7spaxgakssvz59rv6sg9bq7";
        };
      }
      { 
        name = "gazorby/fish-abbreviation-tips";
        src =  pkgs.fetchFromGitHub { 
          owner = "gazorby";
          repo = "fish-abbreviation-tips";
          rev = "8ed76a62bb044ba4ad8e3e6832640178880df485";
          sha256 = "05b5qp7yly7mwsqykjlb79gl24bs6mbqzaj5b3xfn3v2b7apqnqp";
        };
      }
      
    ];

    functions = {
      # TODO: would be nice to walk the folder and add them automatically
      add-to-path = {
        body = (readFn "add-to-path");
        description = "Prepend path to PATH";
      };
      delete-from-path = (readFn "delete-from-path");
      envdown = (readFn "envdown");
      envup = {
        body = (readFn "envup");
        description = "Load environment vars from specified file (default .env). Skips commented (#) lines";
        argumentNames = "file";
      };
      list-user-path = (readFn "list-user-path");
      remove-from-path = (readFn "remove-from-path");
      root = (readFn "root");
      show-path = (readFn "show-path");
      up = {
        description = "Go up levels in the folder hierarchy";
        argumentNames = "levels";
        body = (readFn "up");
      };
    };

    interactiveShellInit = ''
      # Clean fish greeting message
      set fish_greeting

      # Set SHELL env var (like other shells)
      #set -x SHELL /usr/bin/fish
      # Use home-manager nix fish
      set -x SHELL /home/endorama/.nix-profile/bin/fish


      # Are you really try to make me use nano, aren't you? 😜
      set -x EDITOR nvim

      # Fix locale issues with nix-shell
      # https://github.com/NixOS/nixpkgs/issues/8398
      set -x LOCALE_ARCHIVE (readlink ~/.nix-profile/lib/locale)"/locale-archive"

      # set base16_theme "spacemacs"
      # set -x BASE16_THEME $base16_theme
      # NOTE: patch https://github.com/tomyun/base16-fish loading theme only on login shell
      # https://github.com/tomyun/base16-fish/issues/6
      # if test -n "$base16_theme" && status --is-interactive
      #   base16-$base16_theme
      # end

      # fzf.fish & tmux-fish integration
      # https://github.com/PatrickF1/fzf.fish/wiki/Cookbook#how-can-i-open-fzf-in-a-new-tmux-pane
      function fzf --wraps=fzf --description="Use fzf-tmux if in tmux session"
        if set --query TMUX
          fzf-tmux $argv
        else
          command fzf $argv
        end
      end
    '';


    shellAbbrs = {
      dkc = "docker compose";
      ybat = "bat -l yaml";
      k = "kubectl";
      tf = "terraform";
      tfa = "terraform apply";
      tfim = "terraform import";
      tfin = "terraform init";
      tfinit = "terraform init";
      tfp = "terraform plan";
      tfo = "terraform output";
    };

    shellAliases = {
      audio4call = "pacmd set-card-profile bluez_card.9C_0C_35_D0_C2_EE handsfree_head_unit";
      audio4music = "pacmd set-card-profile bluez_card.9C_0C_35_D0_C2_EE a2dp_sink";
      # exa (exa is already aliased to ls)
      l = "ls -l";
      lat = "ls -laT -L 2 -I .git"; # la tree
      latd = "lat -L 5"; # lat deep
      # git
      g = "git";
      # tmuxinator
      mux = "tmuxinator";
      muxc = "tmuxinator close";
      muxo = "tmuxinator start";
      pbcopy = "xclip -selection clipboard";
      powerfull = "sudo /opt/dell/dcc/cctk  --PrimaryBattChargeCfg=Standard";
      powersafe = "sudo /opt/dell/dcc/cctk  --PrimaryBattChargeCfg=Custom:60-80";
      # trash
      rm = "trash";
    };
  };
}
