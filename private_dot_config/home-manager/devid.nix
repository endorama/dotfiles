{ lib
, buildGoModule
, fetchFromGitHub
}:

buildGoModule rec {
  pname = "devid";
  version = "0.3.0";

  src = fetchFromGitHub {
    owner = "endorama";
    repo = "devid";
    rev = "v${version}";
    hash = "sha256-22I6PmtDrGqP/Q11RTdk5m6yhxsE0r/oeJPcZIxX7LM=";
  };

  vendorHash = "sha256-MkzXkS6ukpWd3Et92jugJIAIdJ1oy03X9Jy+opEF9IA=";

  ldflags = [ "-s" "-w" ];

  meta = with lib; {
    description = "Securely manage your developer personas";
    homepage = "https://github.com/endorama/devid";
    license = licenses.asl20;
    maintainers = with maintainers; [ endorama ];
    mainProgram = "devid";
  };
}
