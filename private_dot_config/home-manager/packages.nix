{pkgs, lib, config, ...}:
let
  languages = [
    pkgs.unstable.go
    pkgs.nodejs
    pkgs.ruby_3_3
  ];
  nixtools = [
    pkgs.nix-index # provides: nix-locate
    pkgs.nix-prefetch-git
    # pkgs.nix-direnv
  ];
  nerdfonts = pkgs.nerdfonts.override { fonts = [ "Hack" ]; };
  custompkgs = [
    pkgs.custom.dep-tree
    pkgs.custom.devid
    pkgs.custom.go-licenser
    pkgs.custom.petname
    pkgs.custom.ticker
    pkgs.custom.tickrs
  ];
  workpkgs = [
    (pkgs.google-cloud-sdk.withExtraComponents [pkgs.google-cloud-sdk.components.gke-gcloud-auth-plugin])
    pkgs.awscli2
    pkgs.azure-cli
    pkgs.go-mockery
    pkgs.goreleaser
    pkgs.okta-aws-cli
    pkgs.teleport_14

    pkgs.custom._elastic-package
    pkgs.custom._devenv
  ];
in
{
  options = {
    work_packages.enable = lib.mkEnableOption "enables work packages";
  };
  config = {
    home.packages = languages ++ nixtools ++ [ nerdfonts ] ++ custompkgs ++ workpkgs ++
      # pkgs.lib.optionals (config.work_packages.enable) workpkgs ++ [
      [
      pkgs.devenv

      pkgs.glibcLocales

      # required by vim plugins
      pkgs.code-minimap # https://github.com/wfxr/minimap.vim


      pkgs._1password
      pkgs.age
      pkgs.asciinema
      pkgs.babelfish
      pkgs.bat
      pkgs.cargo
      pkgs.certigo
      pkgs.chezmoi
      pkgs.cointop
      pkgs.cosign
      pkgs.ctlptl
      pkgs.cue
      pkgs.dconf2nix
      pkgs.direnv
      pkgs.discord
      pkgs.dive
      pkgs.docker-compose
      pkgs.docopts
      pkgs.dogdns
      pkgs.doitlive
      pkgs.envchain
      pkgs.fasd
      pkgs.fd
      pkgs.fx
      pkgs.fzf
      pkgs.glow
      pkgs.govulncheck
      pkgs.gradle
      pkgs.graphviz-nox
      pkgs.gum
      pkgs.httpie
      pkgs.jo
      pkgs.jq
      pkgs.kind
      pkgs.kubernetes-helm
      pkgs.lazygit
      pkgs.libfaketime
      pkgs.mage
      pkgs.mycli
      pkgs.unstable.neovim
      pkgs.nerdfix
      pkgs.ollama
      pkgs.pgcli
      pkgs.python39Packages.python-vipaccess
      pkgs.qrencode
      pkgs.ripgrep
      pkgs.shellcheck
      pkgs.silver-searcher
      pkgs.sops
      pkgs.stern
      pkgs.teamocil
      pkgs.terraform
      pkgs.terraform-ls
      pkgs.tfk8s
      pkgs.tflint
      pkgs.tldr
      pkgs.tmuxinator
      pkgs.trash-cli
      pkgs.uv
      pkgs.vagrant
      pkgs.valgrind
      pkgs.vault
      pkgs.vim
      pkgs.yarn
      pkgs.yq-go
      #pkgs.zoom-us

      pkgs.unstable.arduino-cli
      pkgs.unstable.ghq
      pkgs.unstable.go-task
      pkgs.unstable.ghostty
      pkgs.unstable.golangci-lint
      pkgs.unstable.gotestsum
      pkgs.unstable.hugo
      pkgs.unstable.lnav
      pkgs.unstable.nmap
      #pkgs.unstable.poetry
      pkgs.unstable.pre-commit
      # pkgs.unstable.pulumi-bin
      pkgs.unstable.python310Packages.six
      pkgs.unstable.python310Packages.pipx
      # pkgs.unstable.python39Packages.six
      pkgs.unstable.zoxide
    ];
  };
}
