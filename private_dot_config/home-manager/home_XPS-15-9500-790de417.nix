{ config, pkgs, lib, ... }:

# example configs:
# - https://github.com/jonringer/nixpkgs-config
# - https://github.com/yrashk/nix-home/blob/55fc51e1954184e0f5d9a00916963e2ce8b56d21/home.nix
# - https://github.com/nocoolnametom/nixos-configuration/blob/main/home.dist.nix
let
#  gcloudsdk = pkgs.unstable.googlecloudsdk.withExtraComponents([googlecloudsdk.components.local-extract]);
in
{
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  xdg.enable = true;
  targets.genericLinux.enable = true;

  # Sync home-manager and nix-env nixpkgs configs
  # https://github.com/nix-community/home-manager/issues/226
  nixpkgs.config = import ./config.nix;

  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "endorama";
  home.homeDirectory = "/home/endorama";

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "21.05";

  home.sessionVariables = {};

  nixpkgs.overlays = [
    (self: super: { 
      docopts = super.docopts.overrideAttrs (old: {
        src = super.fetchFromGitHub {
          owner = "docopt";
          repo = "docopts";
          rev = "v0.6.4-with-no-mangle-double-dash";
          sha256 = "0zxax0kl8wqpkzmw9ij4qgfhjbk4r7996pjyp9xf5icyk8knp00q";
          };
      });
    }) 
  ];

  # TODO: neovim + plugins
  # https://nixos.wiki/wiki/Neovim
  # https://gist.github.com/nat-418/d76586da7a5d113ab90578ed56069509
  # https://framagit.org/vegaelle/nix-nvim
  imports = [ 
    ./atuin.nix
    ./direnv.nix
    ./conf.d/exa.nix
    ./fish.nix
    ./gh.nix
    ./git.nix
    ./starship.nix
    ./conf.d/tmux.nix

    ./packages.nix
  ];

  # creating custom desktop entries
  # xdg.desktopEntries = {
  #     foo = {
  #       name = "foo";
  #       exec = "Discord";
  #       terminal = false;
  #       icon = "discord";
  #       categories = [ "Network" "InstantMessaging" ];
  #       mimeType = [ "x-scheme-handler/discord" ];
  #       type = "Application";
  #     };
  #   };

  # xdg.dataHome."applications" = {
  #   # Set this to true if you want to be able to write files to the folder
  #   # created by Home Manager. Instead of symlinking the folder, this will symlink
  #   # each file to the destination.
  #   recursive = true;
  #   source = "${pkgs.buildEnv {
  #     name = "home-manager-applications";
  #     paths = config.home.packages;
  #     pathsToLink = "/share/applications";
  # 
  #     postBuild = ''
  #       ls $out/share/applications;
  #       chmod +x -R "$out/share/applications";
  #     '';
  #   }}/share/applications";
  # };
}
