{ config, pkgs, lib, ... }:
{
  programs.git = {
    package = pkgs.unstable.git;
    enable = true;

    userName = "Edoardo Tenani";

    # diff tool
    diff-so-fancy.enable = true;

    aliases = {
      # Custom commands.
      # amend previous commit
      amend = "!f() { if git config user.signingkey; then git commit --amend --sign --reset-author; else git commit --amend; fi }; f";
      # show all branches by last committed to
      branches = "for-each-ref --sort=-committerdate --format=\"%(color:blue)%(authordate:relative)\t%(color:red)%(authorname)\t%(color:white)%(color:bold)%(refname:short)\" refs/remotes";
      # commit with message as argument; sign commit if user.signingkey is set
      cmsg = "!f() { if git config user.signingkey; then git c -S -m \"$@\"; else git c -m \"$@\"; fi }; f";
      # discard all current non-staged changes
      discard = "co- .";
      graph = "log --graph --all --pretty=format:\"%Cred%h%Creset - %Cgreen(%cr)%Creset %s%C(yellow)%d%Creset\" --abbrev-commit --date=relative";
      graph-reflog = "log --graph --all --pretty=format:\"%Cred%h%Creset - %Cgreen(%cr)%Creset %s%C(yellow)%d%Creset\" --abbrev-commit --date=relative --reflog";
      # show commit difference from merge base
      lgcb = "!git log $(git merge-base $(git current-merge-base) $(git current-branch))..HEAD --oneline --decorate";
      # as above with complete logs
      logcb = "!git log $(git merge-base $(git current-merge-base) $(git current-branch))..HEAD --decorate";
      # show commit difference from remote merge base
      lgcr = "!git log $(git merge-base origin/$(git current-merge-base) $(git current-branch))..HEAD --oneline --decorate";
      # as above with complete logs
      logcr = "!git log $(git merge-base origin/$(git current-merge-base) $(git current-branch))..HEAD --decorate";
      # show last commits on brach (default 5)
      lglast = "!f() { git lg -\${1:-5}; }; f";
      mwb = "for-each-ref --sort=-committerdate --count=10 refs/heads/ --format='%(HEAD) %(color:yellow)%(refname:short)%(color:reset) - %(color:red)%(objectname:short)%(color:reset) - %(contents:subject) - %(authorname) (%(color:green)%(committerdate:relative)%(color:reset))'";
      patch = "!git --no-pager diff --no-color --no-prefix";
      psup = "!f() { git push --set-upstream \${1:-origin} $(git current-branch); }; f";
      root = "rev-parse --show-toplevel";
      rbib = "rebase-from-branch-beginning";
      # check which commit added a file
      whatadded = "log --diff-filter=A";
      # check which commit modified a file
      whatchanged = "log --diff-filter=M";
      # check which commit modified a file
      whatremoved = "log --diff-filter=D";
      # print last worked on branches
      wip = "for-each-ref --sort='authordate:iso8601' --format=' %(color:green)%(authordate:relative)%09%(color:white)%(refname:short)' refs/heads";

      # Shortcuts.
      a = "add";
      aa = "add .";
      ap = "add -p";
      apa = "add -Ap";
      b = "branch";
      bd = "branch -d";
      bD = "branch -D";
      c = "commit";
      cf = "commit --fixup";
      #cmsg = "commit -m";
      #scmsg = "commit -m -S";
      ca = "commit --amend";
      can = "commit --amend --no-edit";
      co = "checkout";
      co- = "checkout --";
      cob = "checkout -b";
      cp = "cherry-pick";
      cpa = "cherry-pick --abort";
      cpc = "cherry-pick --continue";
      d = "diff";
      dc = "diff --cached";
      fa = "fetch --all";
      l = "pull";
      lg = "log --oneline";
      lgcbp = "lgcb -p";
      lgcbs = "lgcb --stat";
      lgss = "log --show-signature";
      p = "push";
      pf = "push --force-with-lease";
      pt = "push --tags";
      rb = "rebase";
      rba = "rebase --abort";
      rbc = "rebase --continue";
      rbi = "rebase --interactive --autosquash";
      shp = "stash pop";
      shs = "stash save";
      st = "status -sb";
    };

    # https://tekin.co.uk/2020/10/better-git-diff-output-for-ruby-python-elixir-and-more
    attributes = [
      "*.c     diff=cpp"
      "*.c++   diff=cpp"
      "*.cc    diff=cpp"
      "*.cpp   diff=cpp"
      "*.cs    diff=csharp"
      "*.css   diff=css"
      "*.el    diff=lisp"
      "*.ex    diff=elixir"
      "*.exs   diff=elixir"
      "*.go    diff=golang"
      "*.h     diff=cpp"
      "*.h++   diff=cpp"
      "*.hh    diff=cpp"
      "*.hpp   diff=cpp"
      "*.html  diff=html"
      "*.lisp  diff=lisp"
      "*.m     diff=objc"
      "*.md    diff=markdown"
      "*.mm    diff=objc"
      "*.php   diff=php"
      "*.pl    diff=perl"
      "*.py    diff=python"
      "*.rake  diff=ruby"
      "*.rb    diff=ruby"
      "*.rs    diff=rust"
      "*.xhtml diff=html"
    ];

    includes = [
      {
        condition = "gitdir:~/code/github.com/elastic/";
        contents = {
          commit.gpgsign = true;
          user.email = "edoardo.tenani@elastic.co";
          user.signingkey = "17FA53A59C0EE36E\!";
          user.useConfigOnly = false;
          core.excludesfile = "~/.config/git/ignore";
        };
      }
      {
        condition = "gitdir:~/code/github.com/endorama/";
        contents = {
          user.email = "edoardo.tenani@pm.me";
          user.useConfigOnly = false;
        };
      }
      {
        condition = "gitdir:~/.local/share/chezmoi/";
        contents = {
          user.email = "edoardo.tenani@pm.me";
          user.useConfigOnly = false;
        };
      }
    ];

    extraConfig = {
      commit.template = "/home/endorama/.gitmessage";
      init.defaultBranch = "main";
      pull.rebase = true;
      push.default = "simple";
      rebase.stat = true;
      user.useConfigOnly = true;

      # colours
      color.ui = true;
      color.diff-highlight.oldNormal = "red bold";
      color.diff-highlight.oldHighlight = "red bold 52";
      color.diff-highlight.newNormal = "green bold";
      color.diff-highlight.newHighlight = "green bold 22";
      color.diff.meta = "yellow";
      color.diff.frag = "magenta bold";
      color.diff.commit = "yellow bold";
      color.diff.old = "red bold";
      color.diff.new = "green bold";
      color.diff.whitespace = "red reverse";
      color.blame.highlightRecent = "234, 23 month ago, 235, 22 month ago, 236, 21 month ago, 237, 20 month ago, 238, 19 month ago, 239, 18 month ago, 240, 17 month ago, 241, 16 month ago, 242, 15 month ago, 243, 14 month ago, 244, 13 month ago, 245, 12 month ago, 246, 11 month ago, 247, 10 month ago, 248, 9 month ago, 249, 8 month ago, 250, 7 month ago, 251, 6 month ago, 252, 5 month ago, 253, 4 month ago, 254, 3 month ago, 231, 2 month ago, 230, 1 month ago, 229, 3 weeks ago, 228, 2 weeks ago, 227, 1 week ago, 226";

      # coloured blame
      blame.coloring = "highlightRecent";
      blame.date = "human";

      # merge tool
      merge.tool = "smerge";
      mergetool.smerge.cmd = "smerge mergetool \"$BASE\" \"$LOCAL\" \"$REMOTE\" -o \"$MERGED\"";
      mergetool.smerge.trustExitCode = true;

      # third party configs
      ghq.root = "/home/endorama/code/";
      gitsh.prompt = "%b %c%#%w";
    };
  };
}
