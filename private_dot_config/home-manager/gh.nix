{ pkgs, ... }:
{
  programs.gh = {
    enable = true;
    package = pkgs.unstable.gh;

    gitCredentialHelper.enable = true;

    settings = {
      # NOTE: workaournd for gh data migration in 2.40.0+
      # https://github.com/nix-community/home-manager/issues/4744#issuecomment-1849590426
      version = 1;

      aliases = {
        co = "pr checkout";
        pv = "pr view";
      };

    };
  };
}
