{ pkgs, config, ... }:
{
  programs.tmux = {
    enable = true;

    baseIndex = 1;
    clock24 = true;
    escapeTime = 50;
    keyMode = "vi";
    mouse = true;
    shortcut = "a";

    plugins = with pkgs; [
      # missing powerline (not packaged)
      # tmuxPlugins.prefix-highlight
      # tmuxPlugins.tmux-copycat
      # tmuxPlugins.tmux-pain-control
      # tmuxPlugins.tmux-prefix-highlight
      # tmuxPlugins.tmux-resurrect
      # tmuxPlugins.tmux-logging
      tmuxPlugins.tmux-fzf
      tmuxPlugins.vim-tmux-navigator
      tmuxPlugins.yank
    ];

    extraConfig = ''
    # Required with tmux > 3.3a
    # https://github.com/tmux/tmux/issues/3218#issuecomment-1153404631
    set -g allow-passthrough on

    # show status at the top
    set-option -g status-position top

    # shorten keypress repeat time
    set -sg repeat-time 300

    # vim like copy
    # unbind default copy mode
    unbind [
    # bind vim-like copy mode
    bind escape copy-mode 

    # Split panes with \ and -
    bind \\ split-window -h -c "#{pane_current_path}"
    bind - split-window -v -c "#{pane_current_path}"
    unbind '"'
    unbind %

    # renumber windows when a window is closed
    set -g renumber-windows on

    # Toogle pane synchronization
    bind S setw synchronize-panes \; \
      if-shell '[ #{pane_synchronized} -eq 1 ]' \
          'set -w window-status-style fg=black,bg=yellow ; set -w window-status-current-style fg=yellow,bg=black' \
          'set-window-option -g window-status-current-fg $base09 ; set-window-option -g window-status-current-bg default ; set-window-option -g window-status-current-attr bright'

    source-file ${config.home.homeDirectory}/.config/tmux/endorama.tmuxtheme
    '';
  };
}
