{pkgs, ...}:
{
  programs.atuin = {
    enable = true;
    package = pkgs.unstable.atuin;

    # https://github.com/ellie/atuin/blob/main/docs/config.md
    settings = {
      enter_accept = true;
      filter_mode = "directory";
      search_mode = "fuzzy";
    };
  };

}
