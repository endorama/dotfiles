function envdown -a file -d "Unload environment vars from specified file (default .env). Optionally skips commented (#) lines with --skip-commented"
  # default to .env
  if test -z $file
    set file ".env"
  end
 
  set -l skip_commented false
  for option in $argv
    switch "$option"
      case --skip-commented
        set skip_commented true
    end
  end

  if test -r "$file"
    set -l envs
    while read -l line
      if $skip_commented
        # skip lines starting with #
        if string match -r -q  '^#.*' $line
          continue
        end
      else
        # do not skip lines starting with #
        if string match -r -q  '^#.(?<matched>.*)' $line
          set line $matched
        end
      end

      # split line on first =
      set -l kv (string split -m 1 = -- $line)
      # use $kv[1] as variable name
      set -e $kv[1]
      # store in list for print
      set -a envs $kv[1]
    end < "$file"
    echo "Unloaded "(string join ", " $envs)
  else
    echo "no $file here"
    return 1
  end
end
