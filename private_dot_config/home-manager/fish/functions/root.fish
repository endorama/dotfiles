# A function to get to the conceptual "root" of a  folder tree.
# For example a git root, or the home folder or the log root folder.
# The idea is that this command changes behaviour based on context.
function root
  if git rev-parse --is-inside-work-tree 1>/dev/null 2>&1
    cd (git root)
    return
  end

  echo "this case is not recognized, noop"
end
