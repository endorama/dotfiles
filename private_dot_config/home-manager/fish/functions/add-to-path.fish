set -l usage "USAGE: "(status current-function)" /path/to/add"
set -l count (count $argv)

contains -- $argv $fish_user_paths
    or set -U fish_user_paths $fish_user_paths $argv
echo "Updated PATH: $PATH"

