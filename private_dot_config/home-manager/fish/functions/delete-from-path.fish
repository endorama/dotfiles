
function delete-from-path -d "Delete PATH by index"
    set -l usage "USAGE: "(status current-function)" NUM"
    set -l count (count $argv)

    if test "$count" != "1"
        echo 1>&2 "$usage"
        return
    end

    set --erase --universal fish_user_paths[$argv[1]]
end
