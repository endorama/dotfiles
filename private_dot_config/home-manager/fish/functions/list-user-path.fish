
function list-user-path -d "List paths in PATH by index"
  echo $fish_user_paths | tr " " "\n" | nl
end
