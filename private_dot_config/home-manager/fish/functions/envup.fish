# default to .env
if test -z $file
  set file ".env"
end

if test -r "$file"
  set -l envs
  while read -l line
    # skip lines starting with #
    if string match -r -q  '^#.*' $line
      continue
    end

    # split line on first =
    set -l kv (string split -m 1 = -- $line)
    # use $kv[1] as variable name, the rest as value
    set -gx $kv
    # store in list for print
    set -a envs $kv[1]
  end < "$file"
  echo "Loaded "(string join ", " $envs)
else
  echo "no $file here"
  return 1
end
