# A function to make easier to move up in the folder hierarchy without writing ../../ until you 
# fingers break.
# Inspiration from https://github.com/peterhurford/up.zsh/blob/master/up.plugin.zsh
# function up -d "Go up levels in the folder hierarchy" -a levels;
  if test -z "$levels"
    set levels 1
  end
  
  set upstring (string repeat --count $levels "../")

  cd $upstring
# end
