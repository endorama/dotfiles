
function show-path -d "Print PATH"
    printenv PATH | tr : "\n" | nl
end
