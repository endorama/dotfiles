{ pkgs, lib, config, ...}: {
  options = {
    work_packages.enable = lib.mkEnableOption "enables work packages";
  };

  config = lib.mkIf config.work_packages.enable {
    workpkgs =  [
      (pkgs.google-cloud-sdk.withExtraComponents [pkgs.google-cloud-sdk.components.gke-gcloud-auth-plugin])
      pkgs.awscli2
      pkgs.azure-cli
      pkgs.go-mockery
      pkgs.goreleaser
      pkgs.okta-aws-cli
      pkgs.teleport_14

      pkgs.custom._elastic-package
    ];
  };
}
